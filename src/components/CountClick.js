import { Component } from "react";

class CountClick extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
        }
        // khai báo với bind
        //this.clickHandleEvent = this.clickHandleEvent.bind(this);
    }
    //clickHandleEvent() {
    //this.setState({
    //count: this.state.count + 1,
    //})
    //}
    // khai báo không cần phương thức bind
    // Arrow function không có this = > this tự trỏ đến đối tượng lớn nhất của modules là class
    clickHandleEvent = () => {
        this.setState({
            count: this.state.count + 1,
        })
    }
    render() {
        return (
            <div>
                <button onClick={this.clickHandleEvent}>Click me!</button>
                <p>Count click: {this.state.count} times</p>
            </div>
        )
    }
}
export default CountClick;
